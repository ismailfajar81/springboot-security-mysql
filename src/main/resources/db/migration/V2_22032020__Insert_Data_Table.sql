INSERT INTO user (id, enabled, password, username)
VALUES (1, TRUE, '$2a$04$GR0Lqg9.0KSqaGnOXxkXCOIe3mLumryflpMUgrJAOs1fq6zHBTf4q', 'admin'),
       (2, TRUE, '$2a$04$GR0Lqg9.0KSqaGnOXxkXCOIe3mLumryflpMUgrJAOs1fq6zHBTf4q', 'user');

INSERT INTO authority (id, authority)
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER');

INSERT INTO authorities_users (user_id, authority_id)
VALUES (1, 1),
       (1, 2),
       (2, 2);
-- password
